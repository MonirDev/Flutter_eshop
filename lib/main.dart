import 'package:eshop/app/utils/app_theme.dart';
import 'package:eshop/app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

//Load dotenv file
  await dotenv.load(fileName: Constants.envPath);
  await GetStorage.init();
  runApp(
    ScreenUtilInit(
      designSize: Size(360, 690),
      builder: (context, child) => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: Constants.appName,
        initialRoute: AppPages.initial,
        getPages: AppPages.routes,
        theme: AppThemeData.themeData,
      ),
    ),
  );
}
